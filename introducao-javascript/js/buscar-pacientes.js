var botaoAdicionar = document.querySelector("#buscar-pacientes");

botaoAdicionar.addEventListener("click", function() {


  var xhr = new XMLHttpRequest();

  xhr.open("GET", "https://api-pacientes.herokuapp.com/pacientes");

  // Para pegarmos a resposta quando a requisiçao HTTP
  // voltar precisamos colocar um escutador de evento no próprio
  // XMLHttpRequest, escutando o evento de load
  xhr.addEventListener("load", function() {

    var erroAjax = document.querySelector("#erro-ajax");

    if (xhr.status == 200) {
      erroAjax.classList.add("invisivel");

      var resposta = xhr.responseText;
      var pacientes = JSON.parse(resposta);

      //console.log("Buscando pacientes ...");
      //var resposta = xhr.responseText;
      //console.log(xhr.responseText);

      // Converte String Json para objeto java script
      //var pacientes = JSON.parse(resposta);
      // console.log(pacientes);

      // Adiciona na tabela de pacientes
      pacientes.forEach(function(paciente) {
        adicionaPacienteNaTabela(paciente);
      });

    } else {
      erroAjax.classList.remove("invisivel");
    }
  });

  xhr.send();

});
