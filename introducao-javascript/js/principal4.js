// querySelectorAll traz todos os elementos selecionado.

var pacientes = document.querySelectorAll(".paciente");

for (var i = 0; i < pacientes.length; i++) {
  var paciente = pacientes[i];
  //console.log(pacientes[0].querySelector(".info-nome").textContent);

  var tdPeso = paciente.querySelector(".info-peso");
  var peso = tdPeso.textContent;

  var tdAltura = paciente.querySelector(".info-altura");
  var altura = tdAltura.textContent;

  var tdImc = paciente.querySelector(".info-imc");

  var pesoEhValido = true;
  var alturaEhValida = true;

  if (peso <= 0 || peso >= 1000) {
    pesoEhValido = false;
    tdImc.textContent = "Peso inválido";
    // Não é boa pratica usar o style.backgroundColor
    //paciente.style.backgroundColor="lightcoral";

    // A boa pratica é usar o classList.add chamando o css
    paciente.classList.add("paciente-invalido");
  }

  if (altura <= 0 || altura >= 3.00) {
    alturaEhValida = false;
    tdImc.textContent = "Altura é inválida";
    //paciente.style.backgroundColor="lightcoral";
    paciente.classList.add("paciente-invalido");
  }

  if (pesoEhValido && alturaEhValida) {
    var imc = peso / (altura * altura);
    tdImc.textContent = imc.toFixed(2);
  }
}

var adicionarAdicionar = document.querySelector("#adicionar-paciente");
adicionarAdicionar.addEventListener("click", function(event) {
  // Previne o comportamento padrão dos usuarios no navegador
  event.preventDefault();
  var form = document.querySelector("#form-adiciona");

  var nome = form.nome.value;
  var altura = form.altura.value;
  var peso = form.peso.value;
  var gordura = form.gordura.value;


  // createElement, cria elemento(tag) html
  var pacienteTr = document.createElement("tr")

  var nomeTd = document.createElement("td");
  var alturaTd = document.createElement("td");
  var pesoTd = document.createElement("td");
  var gorduraTd = document.createElement("td");
  var imcTd = document.createElement("td");

  nomeTd.textContent = nome;
  alturaTd.textContent = altura;
  pesoTd.textContent = peso;
  gorduraTd.textContent = gordura;

  //appendChild, adiciona no elemento(tag) html tag filho(outras tag html)
  pacienteTr.appendChild(nomeTd);
  pacienteTr.appendChild(pesoTd);
  pacienteTr.appendChild(alturaTd);
  pacienteTr.appendChild(gorduraTd);

  var tabela = document.querySelector("#tabela-pacientes");

  tabela.appendChild(pacienteTr);


});




// addEventListener é um escutador de evento, ou seja:
// Escutar os eventos do usuario,
// Escutar as interações do usuario na pagina
// Forma 1 de utilizar evento, que é com função anonima
/*var titulo = document.querySelector(".titulo");
titulo.addEventListener("click", function(){
  console.log("Clicou no nome Maria Aparecida usando função anonima");
});
*/

/*
Forma 2 de utilizar evento
var titulo = document.querySelector(".titulo");
titulo.addEventListener("click", mensagem);

function mensagem(){
  console.log("Clicou no nome Maria Aparecida");
}
*/
