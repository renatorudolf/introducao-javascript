// querySelector traz apenas um unico elemento selecionado.

var paciente = document.querySelector("#primeiro-paciente");

var tdPeso = paciente.querySelector(".info-peso");
var peso = tdPeso.textContent;

var tdAltura = paciente.querySelector(".info-altura");
var altura = tdAltura.textContent;

var tdImc = paciente.querySelector(".info-imc");

var pesoEhValido = true;
var alturaEhValida = true;


if (peso <= 0 || peso >= 1000) {
  pesoEhValido = false;
  tdImc.textContent = "Peso inválido";
}

if (altura <= 0 || altura >= 3.00) {
  alturaEhValida = false;
  tdImc.textContent = "Altura é inválida";
}

if (pesoEhValido && alturaEhValida) {
  var imc = peso / (altura * altura);
  tdImc.textContent = imc;
}
